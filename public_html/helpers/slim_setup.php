<?php
use \Slim\Slim;

// Instantiate Slim
$app = new Slim([
  "debug" => true,
  "view"  => new \Slim\Views\Twig(),
]);

// Set up session cookies
$app->add(new \Slim\Middleware\SessionCookie([
  "httponly" => true,
  "name" => "slim_session",
  "secret" => "201730d4278e576b25515bd90c6072d3",
]));

// Set up Twig
$app->config("templates.path", "templates");

$view = $app->view();
$view->parserOptions = [
  "debug" => true,
  "cache" => "templates/cache",
];

// cmd-F: Remove for production
$view->parserExtensions = [
  new Twig_Extension_Debug(),
];

// Twitter API initialisation
\Codebird\Codebird::setConsumerKey('R05zQXxKsBQOs255wkXAFQ', 'rAxkeV9rKS44mmImN9WXaXh1wSDG1bvPyon2CLIWic');
$cb = \Codebird\Codebird::getInstance();

// Set up userdata before dispatch
$app->hook('slim.before.dispatch', function() use ($app) {
  $user = isset($_SESSION['username']) ? $_SESSION['username'] : false;
  $app->view()->setData('username', $user);
});