<?php
date_default_timezone_set("Europe/London");
require_once "vendor/autoload.php";
require_once "includes/slim_setup.php";
require_once "models/Model.php";
require_once "models/User.model.php";
require_once "models/Org.model.php";

// Set up models
$app->container->singleton("userModel", function() {
  return new \UserModel();
});

$app->container->singleton("orgModel", function() {
  return new \UserModel();
});

$app->get("/", function() use ($app) {
  if (!isset($_SESSION["oauth_token"]) || !isset($_SESSION["oauth_token_secret"]) || !isset($_SESSION["username"])) {
    $app->render("not-logged-in.twig");
    return;
  }

  if (!$app->db->getUsersOrg($_SESSION["username"])) {
    $app->render("not-in-org.twig");
    return;
  }

  $app->render("feedback.twig");
});

$app->get("/dump", function() use ($app, $cb) {
  $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
  var_dump($_SESSION);
  var_dump($cb);

  var_dump($cb->account_verifyCredentials());
});

$app->get("/kill", function() use ($app) {
  unset($_SESSION);
  echo "done";
});

// Set up routes
require_once "routes/login.router.php";
//require_once "routes/org.router.php";

$app->run();

