<?php
class Database
{
  /*****************************
   *      Setup / teardown     *
   *****************************/

  private $_pdo;

  public function __construct() {
    try {
      $this->_pdo = new \PDO("mysql:host=127.0.0.1;dbname=staff", "root", "eijonu");
      $this->_pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    } catch (\Exception $e) {
      echo "<h1>Database error</h1><p>There was an issue when trying to connect to the database:</p><pre>".$e->getMessage()."</pre>";
      exit;
    }

    $this->_pdo->query("SET NAMES utf8");
    return $this->_pdo;
  }

  public function __destruct() {
    $this->_pdo = null;
  }
}