<?php
class OrgModel extends Database
{
  public function getOrgs() {
    return $this->_pdo->query("SELECT org_id, org_name, description FROM organisations")->fetchAll(PDO::FETCH_OBJ);
  }

  public function getOrgAndMembers($org) {
    $stmt = $this->_pdo->prepare("SELECT o.org_id, o.org_name, u.user_id FROM organisations o INNER JOIN users u ON u.org_id = o.org_id WHERE o.org_id = ?");
    $stmt->execute([$org]);
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }
}
