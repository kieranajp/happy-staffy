<?php
class UserModel extends Database
{
  public function getUser($username) {
    $stmt = $this->_pdo->prepare("SELECT COUNT(user_id) from users WHERE username = ?");
    $stmt->execute([$username]);
    return $stmt->fetchColumn(0);
  }

  public function saveUser($user) {
    $stmt = $this->_pdo->prepare("INSERT INTO users (username, oauth_provider, oauth_token, oauth_secret, oauth_uid) VALUES (?, ?, ?, ?, ?)");
    $stmt->execute($user);
    return $this->_pdo->lastInsertId();
  }

  public function getUsersOrg($user) {
    $stmt = $this->_pdo->prepare("SELECT o.org_id, o.org_name FROM organisations o INNER JOIN users u ON u.org_id = o.org_id WHERE u.user_id = ?");
    $stmt->execute([$user]);
    return $stmt->fetch(PDO::FETCH_OBJ);
  }
}