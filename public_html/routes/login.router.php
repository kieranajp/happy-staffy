<?php
$app->get("/login(:params)", function($params = null) use ($app, $cb) {
  parse_str(substr($params, 1), $get);

  if (!isset($_SESSION["oauth_token"])) {
    // get the request token
    $reply = $cb->oauth_requestToken(array(
        "oauth_callback" => "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]
    ));

    // store the token
    $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
    $_SESSION["oauth_token"] = $reply->oauth_token;
    $_SESSION["oauth_token_secret"] = $reply->oauth_token_secret;
    $_SESSION["oauth_verify"] = true;

    // redirect to auth website
    $app->redirect($cb->oauth_authorize());
    die();

  } else if (isset($get["oauth_verifier"]) && isset($_SESSION["oauth_verify"])) {
    $cb->setToken($_SESSION["oauth_token"], $_SESSION["oauth_token_secret"]);
    unset($_SESSION["oauth_verify"]);

    // get the access token
    $reply = $cb->oauth_accessToken(array(
        "oauth_verifier" => $get["oauth_verifier"]
    ));

    // store the token (which is different from the request token!)
    $_SESSION["oauth_token"] = $reply->oauth_token;
    $_SESSION["oauth_token_secret"] = $reply->oauth_token_secret;
    $_SESSION["username"] = $reply->screen_name;

    // tktk: save all this shit to the database

    // send to same URL, without oauth GET parameters
    $app->redirect("/login");
    die();
  }

  $app->redirect("/");
});